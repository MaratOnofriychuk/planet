import * as THREE from "three";

export function getCirclesShader() {
    const circlePointsAr=[
        //(main)
        [.662,.775,-.28],
        [.63,.84,-.13],//lux
        [.89,.55,-.2139],
        [.54,.75,.5],//Lond
        [-.2138805, .773827135, .692131996],//usa 2
        [-.7738271,.69213199,.21388055],//usa
        [.25,.33,-.968],//hong
        [.53,-.02,-.92]
    ];

    const impacts = [];
    for (let i = 0; i < circlePointsAr.length; i++) {
        impacts.push({
            impactPosition: new THREE.Vector3(circlePointsAr[i][0],circlePointsAr[i][1],circlePointsAr[i][2]),
            impactMaxRadius: THREE.MathUtils.randFloat(0.0001, 0.0002),
            impactRatio: 0.01
        });
    }
    let uniforms = {
        impacts: {value: impacts}
    }

    const materialCircles = new THREE.MeshBasicMaterial({
        color:0xffffff,
        side: THREE.FrontSide,
        onBeforeCompile: shader => {
            shader.uniforms.impacts = uniforms.impacts;
            shader.vertexShader = `
            struct impact {
              vec3 impactPosition;
              float impactMaxRadius;
              float impactRatio;
            };
            uniform impact impacts[${circlePointsAr.length}];
            attribute vec3 center;
            ${shader.vertexShader}
          `.replace(
                `#include <begin_vertex>`,
                `#include <begin_vertex>
            float finalStep = 0.0;
            for (int i = 0; i < ${circlePointsAr.length};i++){
    
              float dist = distance(center, impacts[i].impactPosition);
              float curRadius = impacts[i].impactMaxRadius * impacts[i].impactRatio/2.;
              float sstep = smoothstep(0., curRadius*1.8, dist) - smoothstep(curRadius - ( .8 * impacts[i].impactRatio ), curRadius, dist);
              sstep *= 1. - impacts[i].impactRatio;
              finalStep += sstep;
    
            }
            finalStep = clamp(finalStep*.5, 0., 1.);
            transformed += normal * finalStep * 0.25;
            `
            );
            //console.log(shader.vertexShader);
            // Этот кусочек кода отвечает за «цветовой» шейдер, который и будет скруглять наш PlaneBufferGeometry
            shader.fragmentShader = shader.fragmentShader.replace(
                `vec4 diffuseColor = vec4( diffuse, opacity );`,
                `
            if (length(vUv - 0.5) > 0.5) discard;
            vec4 diffuseColor = vec4( vec3(.7,.7,.7), .1 );
            `);
        }
    });

    materialCircles.defines = {"USE_UV" : ""};

    return materialCircles;
}
