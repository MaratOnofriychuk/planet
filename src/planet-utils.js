import * as THREE from "three";

export function getPointsByImage(data, width) {
    const array = [];

    for(let y = 0; y < width; y++) {// по оси Y
        for(let x = 0; x < width; x++) {// по оси X
            const a = data[((width*y)+x)*4+3];// берём только n-нные значения
            if(a > 200){
                array.push([x-width,y-width/6])// здесь 6.2 — это как бы «отступ от севера»
            }
        }
    }

    return array;
}

export function get3dPoints(arrayPoints) {
    const lonHelper = new THREE.Object3D();

    const latHelper = new THREE.Object3D();
    lonHelper.add(latHelper);

    const positionHelper = new THREE.Object3D();
    positionHelper.position.z = .5;
    latHelper.add(positionHelper);

    const originHelper = new THREE.Object3D();
    originHelper.position.z=.5;
    positionHelper.add(originHelper);

    const lonFudge=Math.PI*.5;
    const latFudge=Math.PI*-0.135;

    const geometries=[];
    let prewLatX = undefined;
    arrayPoints.map(e=>{
        const geometry=new THREE.PlaneGeometry(0.005,0.005);
        // Позиционирование «кружочков»
        // +15 — вращаем на 15 градусов западнее, хотя это можно было сделать иначе — вращать уже весь объект, а не каждый из «кружочков»
        // degToRad — https://threejs.org/docs/#api/en/math/MathUtils.degToRad
        lonHelper.rotation.y = THREE.MathUtils.degToRad(e[0])+lonFudge+15;
        const w=latHelper.rotation.x = THREE.MathUtils.degToRad(e[1])+latFudge;
        if(w-prewLatX===0){
            originHelper.updateWorldMatrix(true,false);// ЭТА
            geometry.applyMatrix4(originHelper.matrixWorld);// и ЭТА штуки необходимы для обновления позиции отдельного «кружочка»
            // Код ниже для анимирования «бум»
            geometry.setAttribute("center", new THREE.Float32BufferAttribute(geometry.attributes.position.array, 3));
            // Добавим вновь созданный «кружочек» в массив
            geometries.push(geometry);
        }
        prewLatX=w;
    });

    return geometries;
}
