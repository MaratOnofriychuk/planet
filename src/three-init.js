import * as THREE from 'three';
// @ts-ignore
import {OrbitControls} from 'three/addons/controls/OrbitControls';
// @ts-ignore
import TWEEN from 'three/addons/libs/tween.module';

const sizes = {
    width: window.innerWidth,
    height: window.innerHeight,
};

const canvas = document.querySelector('.canvas');

export function setupThreeJs() {
    const scene = new THREE.Scene();

    const camera = new THREE.PerspectiveCamera(
        12,
        window.innerWidth / window.innerHeight,
        .01,
        100
    );
    const renderer = new THREE.WebGLRenderer({
        canvas,
    });
    const commands = [];

    scene.add(camera);

    renderer.setSize(sizes.width, sizes.height);
    renderer.render(scene, camera);

    const controls = new OrbitControls(camera, canvas);
    controls.enableDamping = true;

    const clock = new THREE.Clock();
    let isRun = true;
    const tick = () => {
        const delta = clock.getDelta();

        commands.forEach(c => c(delta));

        controls.update();
        TWEEN.update();

        renderer.render(scene, camera);

        if (isRun) {
            window.requestAnimationFrame(tick);
        }
    };
    tick();

    function resize() {
        sizes.width = window.innerWidth;
        sizes.height = window.innerHeight;

        console.log('1) ', sizes);

        // Обновляем соотношение сторон камеры
        camera.aspect = sizes.width / sizes.height;
        camera.updateProjectionMatrix();

        // Обновляем renderer
        renderer.setSize(sizes.width, sizes.height);
        renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
        renderer.render(scene, camera);
    }
    window.addEventListener('resize', resize);

    const stop = () => {
        scene.clear();
        canvas.remove();
        camera.clear();
        renderer.clear();
        isRun = false;
    };

    return {
        scene,
        canvas,
        camera,
        renderer,
        commands,
        stop,
        resize
    };
}



