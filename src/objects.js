import * as THREE from "three";
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import {downloadImage} from "./materials";
import {get3dPoints, getPointsByImage} from "./planet-utils";
import {getCirclesShader} from "./shaders";
import * as BufferGeometryUtils from "three/examples/jsm/utils/BufferGeometryUtils.js";

export function getPlanet() {
    return new THREE.Mesh(
        new THREE.IcosahedronGeometry(1.0,2),
        new THREE.MeshBasicMaterial({
            opacity: 0,
            transparent: true
        }));
}

export function getPlanetMesh() {
    return new THREE.Mesh(
        new THREE.SphereBufferGeometry(1.0499, 64, 36),
        new THREE.MeshStandardMaterial({color:new THREE.Color(0x091e5a)})
    );
}

export function getImage({ texture, pos, rot }) {
    const meshTexture = new THREE.Mesh(
        new THREE.PlaneGeometry(.235,.235),
        new THREE.MeshBasicMaterial( {
            map: texture,
            side: THREE.DoubleSide,
            alphaTest:.5
        }),
    );

    meshTexture.position.set(...pos);
    meshTexture.rotation.set(...rot);
    meshTexture.scale.set(0,0,0);

    return meshTexture;
}


export function getLightGroup() {
    const lightHolder = new THREE.Group();
    const aLight=new THREE.DirectionalLight(0xffffff,2);

    // Установка позиции для этого света
    aLight.position.set(-1.5,1.7,.7);

    // Прикрепляем к удержателю позиции света, чтобы он дальше не крутился вместе с объектами на сцене
    lightHolder.add(aLight);

    // Второй дополнительный свет
    const aLight2=new THREE.DirectionalLight(0xffffff,2);
    aLight2.position.set(-1.5,0.3,.7);
    lightHolder.add(aLight2);

    return lightHolder;
}

export function getMapPointer({ posCil1, posCir2, main = false }){
    let mSC = null;
    let mainSize = null;
    let color=0x008DFB;

    if (main) {
        mainSize=[.004,.004,.3,3];
        mSC=[.017,24];
        color=0x86c3f9
    } else {
        mainSize=[.002,.002,.16,4]
        mSC=[.01,12]
    }

    const cylinder=new THREE.Mesh(
        new THREE.CylinderBufferGeometry(mainSize[0],mainSize[1],mainSize[2],mainSize[3]),
        new THREE.MeshBasicMaterial({color})
    );

    cylinder.position.set(posCil1[0],posCil1[1],posCil1[2]);

    if(posCir2===''){ return {cylinder} }

    const circle = new THREE.Mesh(
        new THREE.CircleBufferGeometry(mSC[0],mSC[1]),
        new THREE.MeshBasicMaterial({color, side: THREE.DoubleSide})
    );

    circle.position.set(posCir2[0],posCir2[1],posCir2[2]);
    circle.lookAt(new THREE.Vector3());

    return {cylinder, circle}
}

export function getText({ text, pos, rot, size, font, color = 0xffffff }){
    text = new THREE.Mesh(
        new TextGeometry(text,{
            font,
            size,
            height: .04,
            curveSegments: 12,
        }),
        new THREE.MeshBasicMaterial({
            color,
            side:THREE.FrontSide
        })
    );

    text.position.set(pos[0],pos[1],pos[2]);
    text.rotation.set(rot[0],rot[1],rot[2]);

    return text;
}

export async function get3DPointsByImage(path, {width, height}) {
    const canvas = document.createElement('canvas');
    canvas.width = width;
    canvas.height = height;
    canvas.classList.add('tmpCanvas');
    document.body.appendChild(canvas);

    const style = document.createElement('style');
    style.innerText=`.tmpCanvas{position:absolute;z-index:-9;width:0;height:0;overflow:hidden}`;
    document.body.appendChild(style);

    const context = canvas.getContext('2d');
    context.drawImage(
        await downloadImage(path),
        0,
        0,
        width,
        height,
    );

    const meshCircles = new THREE.Mesh(
        BufferGeometryUtils.mergeBufferGeometries(
            get3dPoints(
                getPointsByImage(
                    context.getImageData(0, 0, width, height).data,
                    width
                ),
            ),
            false
        ),
        getCirclesShader()
    );

    meshCircles.scale.set(1.051,1.051,1.051)

    canvas.remove();
    style.remove();

    return meshCircles
}

