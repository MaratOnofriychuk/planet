import anime from "animejs";

export function applyPointAnimation(c1) {
    anime({
        targets:c1.cylinder.scale,
        x:[0,1],
        y:[0,1],
        z:[0,1],
        duration:2000,
        delay:1100,
        easing:'easeOutBounce'
    });

    anime({
        targets:c1.circle.scale,
        x:[0,1],y:[0,1],
        z:[0,1],
        duration:2000,
        easing:'linear'
    });
}

export function applyTimelineTextsAnimation(texts) {
    texts.reduce((p, c) => p.add({
        targets: c.scale,
        x: [0, 1],
        y: [0, 1],
        z: [0,1],
        duration: 600,
        easing: 'linear'
    }), anime.timeline());
}

export function applyImageAnimation(image) {
    anime({
        targets: image.scale,
        x: [0,.7],
        y: [0,.7],
        z: [0,1],
        duration: 600,
        easing: 'linear'
    });
}
