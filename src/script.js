import './style.css';
import {setupThreeJs} from './three-init';

import {applyImageAnimation, applyPointAnimation, applyTimelineTextsAnimation} from "./animations";
import {get3DPointsByImage, getImage, getLightGroup, getMapPointer, getPlanetMesh, getText} from "./objects";

import {downloadTexture, fontPromise} from "./materials";
import * as THREE from "three";
import { MeshLine, MeshLineMaterial } from 'three.meshline';

(async () => {
    const { scene, commands, camera, renderer, resize } = setupThreeJs();

    renderer.setClearColor('#333', 1);

    camera.position.set(10.5,4,-3.5);
    camera.setViewOffset(9, 9, -1, 0, 9, 9);

    // const planet = getPlanet();
    const planetMesh = getPlanetMesh();
    const lightHolder = getLightGroup();

    // scene.add(planet);
    scene.add(planetMesh);
    scene.add(lightHolder);

    commands.push(() => lightHolder.quaternion.copy(camera.quaternion));
    resize();

    const pointer = getMapPointer({
        posCil1: [.66, .95, -.18],
        posCir2: [.662, .8, -.18],
        main: true
    });
    planetMesh.add(pointer.circle);
    planetMesh.add(pointer.cylinder);
    applyPointAnimation(pointer);


    const font = await fontPromise;
    const text1 = getText({
        text: 'One',
        pos: [-.02, .03, 0],
        rot: [-.8, 3, 0],
        size: .05,
        font
    });
    const text2 = getText({
        text: 'Test',
        pos: [-.02, .1, -.1],
        rot: [-.8,3,0],
        size: .05,
        font,
        color: 0xff0000
    });
    pointer.cylinder.add(text1);
    pointer.cylinder.add(text2);
    applyTimelineTextsAnimation([text1, text2])


    const image = getImage({
        texture: await downloadTexture('/icons/pine-tree.png'),
        pos: [.62,1,-.37],
        rot: [0,1.95,0],
    });
    planetMesh.add(image);
    applyImageAnimation(image);


    planetMesh.add(
        await get3DPointsByImage(
            '/icons/map.png',
            {
                width: 360,
                height: 180,
            },
        ),
    );
})();


