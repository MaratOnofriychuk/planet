import {FontLoader} from "three/addons/loaders/FontLoader";
import * as THREE from "three";

export let fontPromise = (function() {
    const fontLoader = new FontLoader();
    return new Promise((res) => {
        fontLoader.load('fonts/font-roboto.json', (f) => {
            res(f);
        });
    });
})();


const loader = new THREE.TextureLoader();
export async function downloadTexture(path) {
    return new Promise((res) => loader.load(path, res));
}

export async function downloadImage(path) {
    const img = new Image();
    img.src = path;

    return new Promise((res) => {
        img.onload = () => { res(img); };
    })
}

